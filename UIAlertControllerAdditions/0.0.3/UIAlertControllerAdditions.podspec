Pod::Spec.new do |s|

  s.name         = "UIAlertControllerAdditions"
  s.version      = "0.0.3"
  s.summary      = "Convenience additions for UIAlertController"
  s.license      = "MIT"
  s.author       = { "Kirill Ushkov" => "kirillushkov2010@gmail.com" }
  s.platform     = :ios
  s.homepage     = "https://bitbucket.org/itomych"

  s.source       = { :git => "https://kirill_ushkov@bitbucket.org/kirill_ushkov/uialertcontrolleradditions.git", :tag => "0.0.3" }
  s.source_files = "UIAlertControllerAdditions/**/*.{h,m}"
  s.public_header_files = "UIAlertControllerAdditions/**/*.h"

  s.frameworks  = "UIKit"

end
