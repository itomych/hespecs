Pod::Spec.new do |spec|
    spec.name         = "HETests"
    spec.version      = "0.1.0"
    spec.summary      = "Testing helper library"
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HETests.git", :tag => spec.version }
    spec.requires_arc = true
    spec.platform     = :ios
    spec.frameworks   = 'XCTest'

    spec.source_files = 'HETests/**/*.{h,m}'
    spec.public_header_files = 'HETests/**/*.h'
end
