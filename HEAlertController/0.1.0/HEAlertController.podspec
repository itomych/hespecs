Pod::Spec.new do |spec|
    spec.name         = "HEAlertController"
    spec.version      = "0.1.0"
    spec.summary      = "The HEAlertController extends UIAlertController to make it possible to present it from any class using \"show\" method."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HEAlertController.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'UIKit'

    spec.source_files = 'HEAlertController/**/*.{h,m}'

    spec.public_header_files = 'HEAlertController/**/*.h'

    spec.ios.deployment_target = '8.0'
end
