Pod::Spec.new do |spec|
    spec.name         = "HECameraViewController"
    spec.version      = "0.1.2"
    spec.summary      = "A camera view controller build on top of AVFoundation"
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HECameraViewController.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'AVFoundation', 'UIKit'

    spec.source_files = 'HECameraViewController/**/*.{h,m}'

    spec.public_header_files = 'HECameraViewController/**/*.h'

    spec.ios.deployment_target = '6.0'
end
