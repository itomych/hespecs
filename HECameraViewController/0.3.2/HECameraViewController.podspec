Pod::Spec.new do |spec|
    spec.name         = "HECameraViewController"
    spec.version      = "0.3.2"
    spec.summary      = "A camera view controller build on top of AVFoundation"
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HECameraViewController.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'AVFoundation', 'UIKit'

    spec.default_subspec = "HECameraViewController"

    spec.subspec 'HECameraView' do |subspec|
        subspec.source_files = 'HECameraViewController/HECameraView.{h,m}'
        subspec.public_header_files = 'HECameraViewController/HECameraView.h'
        subspec.ios.deployment_target = '6.0'
    end

    spec.subspec 'HECameraViewController' do |subspec|
        subspec.source_files = 'HECameraViewController/HECameraViewController.{h,m}',
                               'HECameraViewController/HECameraViewController_Private.h'
        subspec.public_header_files = 'HECameraViewController/HECameraViewController.h'
        subspec.ios.deployment_target = '6.0'
        subspec.dependency 'HECameraViewController/HECameraView'
    end

    spec.subspec 'HECaptureStillImageOutput' do |subspec|
        subspec.source_files = 'HECameraViewController/HECameraViewController+HECaptureStillImageOutput.{h,m}'
        subspec.public_header_files = 'HECameraViewController/HECameraViewController+HECaptureStillImageOutput.h'
        subspec.ios.deployment_target = '6.0'
        subspec.dependency 'HECameraViewController/HECameraViewController'
    end

    spec.subspec 'HEStillImageViewController' do |subspec|
        subspec.source_files = 'HECameraViewController/HEStillImageViewController.{h,m}'
        subspec.public_header_files = 'HECameraViewController/HEStillImageViewController.h'
        subspec.ios.deployment_target = '6.0'
        subspec.dependency 'HECameraViewController/HECaptureStillImageOutput'
    end

    spec.subspec 'HECaptureDeviceInputSwitch' do |subspec|
        subspec.source_files = 'HECameraViewController/AVCaptureDevice+HECaptureDevicePosition.{h,m}',
                               'HECameraViewController/HECameraViewController+HECaptureDeviceInputSwitch.{h,m}'
        subspec.public_header_files = 'HECameraViewController/HECameraViewController+HECaptureDeviceInputSwitch.h'
        subspec.ios.deployment_target = '6.0'
        subspec.dependency 'HECameraViewController/HECameraViewController'
    end

    spec.subspec 'HECaptureDeviceModesSwitch' do |subspec|
        subspec.source_files = 'HECameraViewController/AVCaptureDevice+HECaptureDeviceModesSwitch.{h,m}'
        subspec.public_header_files = 'HECameraViewController/AVCaptureDevice+HECaptureDeviceModesSwitch.h'
        subspec.ios.deployment_target = '6.0'
    end
end
