Pod::Spec.new do |spec|
    spec.name         = "HECameraViewController"
    spec.version      = "0.2.0"
    spec.summary      = "A camera build on top of AVFoundation"
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HECameraViewController.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'AVFoundation', 'UIKit'

    spec.default_subspec = "HECameraViewController"

    spec.subspec 'HECameraView' do |view|
        view.source_files = 'HECameraViewController/HECameraView.{h,m}'
        view.public_header_files = 'HECameraViewController/HECameraView.h'
    end

    spec.subspec 'HECameraViewController' do |cameraController|
        cameraController.dependency 'HECameraViewController/HECameraView'

        cameraController.source_files = 'HECameraViewController/HECameraViewController.{h,m}'
        cameraController.public_header_files = 'HECameraViewController/HECameraViewController.h'

        cameraController.ios.deployment_target      = '8.0'
    end

    spec.subspec 'HECameraViewController+AVCaptureStillImageOutput' do |stillImage|
        stillImage.dependency 'HECameraViewController/HECameraViewController'

        stillImage.source_files = 'HECameraViewController/HECameraViewController+AVCaptureStillImageOutput.{h,m}'
        stillImage.public_header_files = 'HECameraViewController/HECameraViewController+AVCaptureStillImageOutput.h'

        stillImage.ios.deployment_target      = '8.0'
    end

    spec.subspec 'HEStillImageViewController' do |takePhoto|
        takePhoto.dependency 'HECameraViewController/HECameraViewController+AVCaptureStillImageOutput'

        takePhoto.source_files = 'HECameraViewController/HEStillImageViewController.{h,m}'
        takePhoto.public_header_files = 'HECameraViewController/HEStillImageViewController.h'

        takePhoto.ios.deployment_target      = '8.0'
    end
end
