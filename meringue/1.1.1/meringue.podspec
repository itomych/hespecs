
Pod::Spec.new do |s|

  s.name         = "meringue"
  s.version      = "1.1.1"
  s.summary      = "Meringue for ios application."

  s.description  = <<-DESC
                      Meringue for ios appliction, writen for private use iTomich studio development 
                   DESC
  s.homepage     = "https://bitbucket.org/itomych/meringue-ios"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "iTomych studio" => "office@itomy.ch" }
  s.platform     = :ios, "6.0"
  s.source       = { :git => "https://bitbucket.org/itomych/meringue-ios/meringue-ios.git", :tag => "#{s.version}" }
  s.source_files  = "meringue/*.h", "meringue/Models/*.h", "meringue/Models/Base/*.h", "meringue/Networking/*.h", "meringue/Storage/*.h"

  s.public_header_files = "meringue/*.h", "meringue/Models/*.h", "meringue/Models/Base/*.h", "meringue/Networking/*.h", "meringue/Storage/*.h"
  s.resource_bundle = { "Meringue" => "meringue/Models/Meringue.xcdatamodeld" }
  #s.preserve_paths = "libMeringue.a"
  s.ios.vendored_libraries = "libMeringue.a"
  s.framework  = "Foundation"
  s.library = "Meringue"
  s.requires_arc = true
  s.dependency 'hextensions', '~> 1.0'
  s.dependency 'AFNetworking', '~> 2.5.4'

end
