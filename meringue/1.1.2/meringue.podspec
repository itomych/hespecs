Pod::Spec.new do |s|
  s.name         = "meringue"
  s.version      = "1.1.2"
  s.summary      = "Meringue for ios application."

  s.description  = <<-DESC
                      Meringue for ios appliction, writen for private use itomich studio development
                   DESC

  s.homepage     = "https://bitbucket.org/itomych/meringue-ios"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "itomych studio" => "office@itomy.ch" }
  s.platform     = :ios, "6.0"
  s.source       = { :git => "https://bitbucket.org/itomych/meringue-ios/meringue-ios.git", :tag => s.version }

  s.source_files        = "meringue/**/*.h"
  s.public_header_files = "meringue/**/*.h"
  s.resource_bundle     = { "Meringue" => "meringue/Models/Meringue.xcdatamodeld" }

  s.ios.vendored_libraries = "libMeringue.a"
  s.framework  = "Foundation"
  s.library = "Meringue"
  s.requires_arc = true
  s.dependency 'AFNetworking', '>= 2.5.4', '< 4'
end
