Pod::Spec.new do |spec|
    spec.name         = "HEIBExtendedButton"
    spec.version      = "0.1.0"
    spec.summary      = "The HEIBExtendedButton extends UIButton to make it possible to change some internal properties directly from the Interface Builder."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HEIBExtendedButton.git", :tag => "0.1.0" }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'UIKit'

    spec.source_files = 'HEIBExtendedButton/**/*.{h,m}'

    spec.public_header_files = 'HEIBExtendedButton/**/*.h'

    spec.ios.deployment_target = '6.0'
end
