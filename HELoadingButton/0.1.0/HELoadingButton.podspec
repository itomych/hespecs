Pod::Spec.new do |spec|
    spec.name         = "HELoadingButton"
    spec.version      = "0.1.0"
    spec.summary      = "UIButton subclass with an activity indicator"
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HELoadingButton.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'UIKit'

    spec.source_files = 'HELoadingButton/**/*.{h,m}'
    spec.public_header_files = 'HELoadingButton/**/*.h'

    spec.ios.deployment_target = '5.1.1'
end
