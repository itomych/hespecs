Pod::Spec.new do |spec|
    spec.name         = "HESwipeViewController"
    spec.version      = "0.1.0"
    spec.summary      = "The HESwipeViewController class creates a controller object that manages a swipe view."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HESwipeViewController.git", :tag => "0.1.0" }
    spec.platform     = :ios
    spec.requires_arc = true
    spec.frameworks   = 'UIKit'

    spec.source_files = 'HESwipeViewController/**/*.{h,m}'

    spec.public_header_files = 'HESwipeViewController/**/*.h'

    spec.dependency 'SwipeView', '~> 1.3'
end
