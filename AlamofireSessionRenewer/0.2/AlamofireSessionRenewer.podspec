Pod::Spec.new do |spec|
    spec.name         = "AlamofireSessionRenewer"
    spec.version      = "0.2"
    spec.summary      = "..."
    spec.homepage     = "https://github.com/dashdevs"
    spec.license      = { :type => 'MIT', :file => 'LICENSE' }
    spec.author       = { "DashDevs LLC" => "hello@dashdevs.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/AlamofireSessionRenewer.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true

    spec.source_files = 'AlamofireSessionRenewer/**/*.{swift}'

    spec.ios.deployment_target = '10.0'
    spec.osx.deployment_target = '10.12'
    spec.tvos.deployment_target = '10.0'
    spec.watchos.deployment_target = '3.0'
    
    spec.swift_versions = ['5.0', '5.1']

    spec.dependency 'Alamofire', '~> 4.9.1'
end
