Pod::Spec.new do |spec|
    spec.name         = "HENetworking"
    spec.version      = "0.2.1"
    spec.summary      = "The HENetworking categories make it easy to perform post-processing of the data."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HENetworking.git", :tag => spec.version }
    spec.platform     = :ios
    spec.requires_arc = true

    spec.source_files = 'HENetworking/**/*.{h,m}'
    spec.public_header_files = 'HENetworking/**/*.h'

    spec.ios.deployment_target      = '7.0'
    spec.osx.deployment_target      = '10.9'
    spec.watchos.deployment_target  = '2.0'
    spec.tvos.deployment_target     = '9.0'

    spec.dependency 'AFNetworking/NSURLSession', '~> 3.0'
end
