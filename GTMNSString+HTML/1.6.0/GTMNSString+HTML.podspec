Pod::Spec.new do |s|
    s.name = 'GTMNSString+HTML'
    s.version = '1.6.0'
    s.license = {
        :type => 'Apache License, Version 2.0',
        :text => 'Copyright (c) 2010 Google Inc.\n\nLicensed under the Apache License, Version 2.0 (the \"License\");\n you may not use this file except in compliance with the License.\n You may obtain a copy of the License at\n\n http://www.apache.org/licenses/LICENSE-2.0\n\n Unless required by applicable law or agreed to in writing, software\n distributed under the License is distributed on an \"AS IS\" BASIS,\n WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n See the License for the specific language governing permissions and\n limitations under the License.\n'
    }
    s.summary = 'Utilities for NSStrings containing HTML.'
    s.description = 'Dealing with NSStrings that contain HTML.'
    s.homepage = 'http://google-toolbox-for-mac.googlecode.com'
    s.author = 'Google Inc.'
    s.source = {
        :git => 'https://github.com/google/google-toolbox-for-mac.git',
        :tag => 'google-toolbox-for-mac-1.6.0'
    }
    s.ios.deployment_target = '5.0'
    s.osx.deployment_target = '10.7'
    s.requires_arc = false
    s.source_files = 'GTMDefines.h', 'Foundation/GTMNSString+HTML.{h,m}'
end
