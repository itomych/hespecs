
Pod::Spec.new do |s|
  s.name         = "HEGunshotDetector"
  s.version      = "1.0.7"
  s.license      = "MIT"
  s.summary      = "Library for gunshots detection."

  s.homepage     = "https://bitbucket.org/itomych"
  s.author             = { "Anatoliy klimenko" => "anatolijklimenko@itomy.ch" }
  s.source       = { :git => "https://bitbucket.org/itomych/hegunshotdetector.git", :tag => "#{s.version}"}
  s.requires_arc   = true

  s.platform     = :ios, "9.0"
  s.source_files  = "HEGunshotDetector/*.{h,m}", "HEGunshotDetector/CFARDetector/*.{h,m}", "HEGunshotDetector/DigitalBiqudFilter/*.{h,m}", "HEGunshotDetector/FIRFilter/*.{h,m}", "HEGunshotDetector/Models/*.{h,m}", "HEGunshotDetector/PeakDetector/*.{h,m}", "HEGunshotDetector/ToneDetector/*.{h,m}", "HEVideoProcessing/*.{h,m}", "HEVideoProcessing/Layers/*.{h,m}", "HEVideoProcessing/Errors/*.{h,m}"

  # s.public_header_files = "HEGunshotDetector/*.h", "HEGunshotDetector/CFARDetector/*.h", "HEGunshotDetector/DigitalBiqudFilter/*.h", "HEGunshotDetector/FIRFilter/*.h", "HEGunshotDetector/Models/*.h", "HEGunshotDetector/PeakDetector/*.h", "HEGunshotDetector/ToneDetector/*.h",  "HEVideoProcessing/*.h", "HEVideoProcessing/Errors/*.h", "HEVideoProcessing/Layers/*.h"

  s.resources = "HEGunshotDetector/Sample/sample.dat"

end
