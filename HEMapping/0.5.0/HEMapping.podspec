Pod::Spec.new do |spec|
    spec.name         = "HEMapping"
    spec.version      = "0.5.0"
    spec.summary      = "The HEMapping protocol declares methods that a class must implement so that instances of that class can map attributes."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HEMapping.git", :tag => spec.version }
    spec.requires_arc = true

    spec.default_subspec = "Core"

    spec.subspec 'Core' do |core|
        core.source_files = 'HEMapping/**/*.{h,m}'
        core.public_header_files = 'HEMapping/**/*.h'
    end

    spec.subspec 'HEObjectMapper+HENetworking' do |handler|
        handler.dependency 'HEMapping/Core'
        handler.dependency 'HENetworking', '~> 0.2.0'

        handler.source_files = 'HEObjectMapper+HENetworking/**/*.{h,m}'
        handler.public_header_files = 'HEObjectMapper+HENetworking/**/*.h'

        handler.ios.deployment_target      = '7.0'
        handler.osx.deployment_target      = '10.9'
        handler.watchos.deployment_target  = '2.0'
        handler.tvos.deployment_target     = '9.0'
    end

    spec.subspec 'HENetworkFetchController' do |fetch|
        fetch.dependency 'HEMapping/Core'
        fetch.dependency 'HEMapping/HEObjectMapper+HENetworking'

        fetch.source_files = 'HENetworkFetchController/**/*.{h,m}'
        fetch.public_header_files = 'HENetworkFetchController/**/*.h'

        fetch.ios.deployment_target      = '7.0'
        fetch.osx.deployment_target      = '10.9'
        fetch.watchos.deployment_target  = '2.0'
        fetch.tvos.deployment_target     = '9.0'
    end

    spec.subspec 'HENetworkObject' do |object|
        object.dependency 'HEMapping/Core'
        object.dependency 'AFNetworking/NSURLSession', '~> 3.0'

        object.source_files = 'HENetworkObject/**/*.{h,m}'
        object.public_header_files = 'HENetworkObject/**/*.h'

        object.ios.deployment_target      = '7.0'
        object.osx.deployment_target      = '10.9'
        object.watchos.deployment_target  = '2.0'
        object.tvos.deployment_target     = '9.0'
    end
end
