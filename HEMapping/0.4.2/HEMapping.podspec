Pod::Spec.new do |spec|
    spec.name         = "HEMapping"
    spec.version      = "0.4.2"
    spec.summary      = "The HEMapping protocol declares methods that a class must implement so that instances of that class can map attributes."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HEMapping.git", :tag => spec.version }
    spec.requires_arc = true

    spec.default_subspec = "Core"

    spec.subspec 'Core' do |core|
        core.source_files = 'HEMapping/**/*.{h,m}'
        core.public_header_files = 'HEMapping/**/*.h'
    end

    spec.subspec 'HENetworking' do |network|
        network.dependency 'HEMapping/Core'
        network.dependency 'AFNetworking/NSURLSession', '~> 3.0'

        network.source_files = 'HENetworking/**/*.{h,m}'
        network.public_header_files = 'HENetworking/**/*.h'

        network.ios.deployment_target      = '7.0'
        network.osx.deployment_target      = '10.9'
        network.watchos.deployment_target  = '2.0'
        network.tvos.deployment_target     = '9.0'
    end

    spec.subspec 'HEObjectMapper+HENetworking' do |handler|
        handler.dependency 'HEMapping/HENetworking'
        handler.dependency 'AFNetworking+HEHandling', '~> 0.1.0'

        handler.source_files = 'HEObjectMapper+HENetworking/**/*.{h,m}'
        handler.public_header_files = 'HEObjectMapper+HENetworking/**/*.h'

        handler.ios.deployment_target      = '7.0'
        handler.osx.deployment_target      = '10.9'
        handler.watchos.deployment_target  = '2.0'
        handler.tvos.deployment_target     = '9.0'
    end
end
