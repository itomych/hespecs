Pod::Spec.new do |spec|
    spec.name         = "HEMapping"
    spec.version      = "0.1.0"
    spec.summary      = "The HEMapping protocol declares methods that a class must implement so that instances of that class can map attributes."
    spec.homepage     = "https://bitbucket.org/itomych"
    spec.license      = 'MIT'
    spec.author       = { "How Else" => "elseisgone@gmail.com" }
    spec.source       = { :git => "https://bitbucket.org/itomych/HEMapping.git", :tag => "0.1.0" }
    spec.platform     = :ios
    spec.requires_arc = true

    spec.source_files = 'HEMapping/**/*.{h,m}'

    spec.public_header_files = 'HEMapping/**/*.h'
end
