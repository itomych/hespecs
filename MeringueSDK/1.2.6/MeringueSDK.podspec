Pod::Spec.new do |s|
  s.name         = "MeringueSDK"
  s.version      = "1.2.6"
  s.summary      = "MeringueSDK for iOS applications"

  s.description  = <<-DESC
                      Meringue for ios appliction, writen for private use itomich studio development
                   DESC

  s.homepage     = "https://bitbucket.org/itomych/meringue-ios"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "itomych studio" => "office@itomy.ch" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/itomych/meringue-ios/meringue-ios.git", :tag => s.version }

  s.public_header_files = "meringue/**/*.h"
  s.source_files        = "meringue/**/*.{h,m}"
  s.resource_bundle     = { "Meringue" => "meringue/Models/Meringue.xcdatamodeld" }

  s.framework  = "Foundation"
  s.requires_arc = true

  s.dependency 'AFNetworking', '>= 2.5.4', '< 4'
  s.dependency 'hextensions/NSFoundation+Networking', '~> 2.2'
end
