Pod::Spec.new do |s|
  s.name         = "MeringueSDK"
  s.version      = "1.3"
  s.summary      = "MeringueSDK for iOS applications"

  s.description  = <<-DESC
                      Meringue for ios appliction, writen for private use itomich studio development
                   DESC

  s.homepage     = "https://bitbucket.org/itomych/meringue-ios"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "itomych studio" => "office@itomy.ch" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/itomych/meringue-ios/meringue-ios.git", :tag => s.version }

  s.framework = "Foundation"
  s.requires_arc = true

  s.dependency 'AFNetworking/NSURLSession', '>= 2.5.4', '< 4'
  s.dependency 'hextensions/NSFoundation+Networking', '~> 2.2'

  s.subspec 'Core' do |subspec|
    subspec.source_files = 'meringue/Core/*.{h,m}'
    subspec.public_header_files = 'meringue/Core/*.h'
    subspec.resource_bundle = { 'Meringue' => 'meringue/Meringue.xcdatamodeld' }
  end

  s.subspec 'Inbox' do |subspec|
    subspec.source_files = 'meringue/Inbox/*.{h,m}'
    subspec.public_header_files = 'meringue/Inbox/*.h'
    subspec.dependency 'MeringueSDK/Core'
  end

  s.subspec 'Assets' do |subspec|
    subspec.source_files = 'meringue/Assets/*.{h,m}'
    subspec.public_header_files = 'meringue/Assets/*.h'
    subspec.dependency 'MeringueSDK/Core'
  end

  s.subspec 'Places' do |subspec|
    subspec.source_files = 'meringue/Places/*.{h,m}'
    subspec.public_header_files = 'meringue/Places/*.h'
    subspec.dependency 'MeringueSDK/Core'
  end

  s.subspec 'Products' do |subspec|
    subspec.source_files = 'meringue/Products/*.{h,m}'
    subspec.public_header_files = 'meringue/Products/*.h'
    subspec.dependency 'MeringueSDK/Assets'
  end
end
