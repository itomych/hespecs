@version = "1.0.0"

Pod::Spec.new do |s|
  s.name         		= "openssl"
  s.version      		= @version
  s.summary      		= "Frameworked version of openssl"
  s.homepage        = "https://itomychstudio.com"
  s.license         = { :type => 'MIT', :file => 'LICENSE' }
  s.author       		= { "iTomych Studio" => "office@itomy.ch" }
  s.source          = { :git => "https://bitbucket.org/itomych/openssl.git", :tag => s.version }

  s.platform     		= :ios, "8.0"
  s.requires_arc 		= true

  # s.frameworks      = 'Security'
  # s.libraries       = 'icucore', 'z', 'iconv'

  s.prepare_command = <<-CMD
      Scripts/download.sh
  CMD

  s.preserve_paths = ['openssl.framework']

  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/"', 'HEADER_SEARCH_PATHS' => '"$(PODS_ROOT)/openssl.framework/Headers"' } 

end